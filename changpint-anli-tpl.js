var tplData = [
   {
    "type": '1', // 1是 前沿动态， 2是应用案例， 3应用感悟
    "id": 8,
    "name": "由“线下”到“线上”  从“封闭”到“开放”—来自北京市昌平四中国佳老师的一个生动案例",
    "teacherName": "刘涛",
    "subject": "",
    "grade": "",
    "schoolName": "特约记者",
    "intro": "周记内容难以分享，学生缺乏写作动力?批改周期长，师生交流缺乏时效性?北京市昌平四中国佳老师利用“虚拟学校”平台开展了“周记写作”的教学实验，活动收到了可喜的成果。",
    "folderName": 'dt001',
    "imgPath": "中学/fscommand/main/dt001/img/fm.jpg",
    "moduleUrl": "中学/fscommand/main/dt001/01.htm",
    "mudule": [], 
  },
  {
    "type": '1', // 1是 前沿动态， 2是应用案例， 3应用感悟
    "id": 9,
    "name": "御风而行  乘势而为——北京市昌平一中吴丽梅主任专访",
    "teacherName": "曹志勇",
    "subject": "",
    "grade": "",
    "schoolName": "特约记者",
    "intro": "吴丽梅，昌平区第一中学教师，语文教研组长，教务处副主任，昌平区学科带头人。探索从课本出发学习讨论，课上解决疑难问题，进而扩展课外材料，进一步巩固课本上的内容，最终，落笔到写作的应用模式。目前，已有已有3篇学生作品发表到报纸上。",
    "folderName": 'dt002',
    "imgPath": "中学/fscommand/main/dt002/img/fm.jpg",
    "moduleUrl": "中学/fscommand/main/dt002/01.htm",
    "mudule": [], 
  },
  {
    "type": '3', // 1是 前沿动态， 2是应用案例， 3应用感悟
    "id": 1,
    "name": "凡有感动，皆为真实；凡有启示，就不成空 ——虚拟课堂使用心得",
    "teacherName": "国  佳",
    "subject": "语文",
    "grade": "初中",
    "schoolName": "昌平四中",
    "folderName": 'yj001',
    "imgPath": "中学/fscommand/main/yj001/img/fm.jpg",
    "intro": "作为新时代课堂模式的改革方式，虚拟课堂成为应用在各学科学习过程中的教学手段，它用独特的形态完善了我们在现实课堂中的教学，于我于学生都受益匪浅。",
    "moduleUrl": "中学/fscommand/main/yj001/01.htm",
    "mudule": [], 
  },
  {
    "type": '3', // 1是 前沿动态， 2是应用案例， 3应用感悟
    "id": 2,
    "name": "燃一盏明灯，照你我前行——“虚拟学校”初步探索心得",
    "teacherName": "吕梦雅",
    "subject": "语文",
    "grade": "初中",
    "schoolName": "北京市一六一中学回龙观学校",
    "folderName": 'yj002',
    "imgPath": "中学/fscommand/main/yj002/img/fm.jpg",
    "moduleUrl": "中学/fscommand/main/yj002/01.htm",
    "intro": "语文学科“精准发力”即要求我们对原有的教学内容做减法，不需要面面俱到，要首先抓好一节或几节课，创造几个案例，也使得我真正借力“虚拟学校”这盏明灯，渐渐踏上了语文教学探索的正途。",
    "mudule": [], 
  },
   {
    "type": '3', // 1是 前沿动态， 2是应用案例， 3应用感悟
    "id": 3,
    "name": "做幸福的老师，教快乐的学生————我与“虚拟学校”的不解之缘",
    "teacherName": "王晓天",
    "subject": "英语",
    "grade": "初中",
    "schoolName": "长陵学校",
    "folderName": 'yj003',
    "imgPath": "中学/fscommand/main/yj003/img/fm.jpg",
    "moduleUrl": "中学/fscommand/main/yj003/01.htm",
    "intro": "实践证明，“虚拟学校”教学平台在英语教学中的运用能给课堂教学注入新的生机和活力，既能丰富教学形式，又能优化教学过程。",
    "mudule": [], 
  },
   {
    "type": '3', // 1是 前沿动态， 2是应用案例， 3应用感悟
    "id": 3,
    "name": "翼，唯有飞翔才能成长——浅谈“虚拟学校”在初中语文学科中的应用",
    "teacherName": "付云菲",
    "subject": "语文",
    "grade": "初中",
    "schoolName": "首都师范大学附属中学昌平学校",
    "folderName": 'yj004',
    "imgPath": "中学/fscommand/main/yj004/img/fm.jpg",
    "moduleUrl": "中学/fscommand/main/yj004/01.htm",
    "intro": "语文素养的习得就如同春风化雨一般，在潜移默化中改变学生的语文学习习惯，固化语文核心素养。让学生在参与中体验，在分享中收获，在互动中完善，在活动中成长真正意义上有所受益。虚拟学校教学平台是一个很好的工具，熟练地运用到我的初中语文教学中，就如同如虎添翼，但是有翼却还是不够，唯有飞翔才能使学生真正意义的成长。",
    "mudule": [], 
  },
  {
    "type": '3', // 1是 前沿动态， 2是应用案例， 3应用感悟
    "id": 3,
    "name": "漫步虚拟起微澜，阅读写作开并蒂——运用虚拟学校平台进行语文教学的探索",
    "teacherName": "曹敏",
    "subject": "语文",
    "grade": "高中",
    "schoolName": "首都师范大学附属中学昌平学校",
    "folderName": 'yj005',
    "imgPath": "中学/fscommand/main/yj005/img/fm.jpg",
    "moduleUrl": "中学/fscommand/main/yj005/01.htm",
    "intro": "“虚拟学校”的使用恰恰契合了当今语文教学的价值理念：充分调动学生的积极性，挖掘学生的内在潜力，多读多写，展现他们的才学智慧。精准发力，即在教学中找到合适的切入点，让虚拟学校去做传统现实课堂无法做到的事情，发挥它的巨大优势。",
    "mudule": [], 
  },
   {
    "type": '3', // 1是 前沿动态， 2是应用案例， 3应用感悟
    "id": 3,
    "name": "借助虚拟平台，提升思维品质——基于虚拟学校的高中英语课外阅读课例析",
    "teacherName": "张晓丽",
    "subject": "英语",
    "grade": "高中",
    "schoolName": "首都师范大学附属中学昌平学校",
    "folderName": 'yj006',
    "imgPath": "中学/fscommand/main/yj006/img/fm.jpg",
    "moduleUrl": "中学/fscommand/main/yj006/01.htm",
    "intro": "通过对一节课外阅读课的评析，探索教师如何在课外阅读教学中重点发展学生的逻辑思维，批判性思维和创新等思维品质。该课利用虚拟学校平台进行英语拓展阅读教学，设计了基于文本的情景再现环节，考查学生对文本的深层理解以及引发的思考。基于虚拟平台讨论功能设计了改写故事结局并分角色朗读，录制视频上传展示活动，讨论现实生活中真实比赛的重要性，引导学生依据课堂习得，建立正确面对和处理生活事件的意识。最终引导学生在深度阅读的基础上提升交流、合作、审辨思维、创造力四种能力。",
    "mudule": [], 
  },
  {
    "type": '2', // 1是 前沿动态， 2是应用案例， 3应用感悟
    "id": 4,
    "name": "shopping for food",
    "teacherName": "王晓天",
    "subject": "英语",
    "grade": "初中",
    "schoolName": "长陵学校",
    "folderName": 'al001',
    "imgPath": "中学/fscommand/main/al001/main/img/fm.jpg",
    "intro": "本节课的优点是听的环节整体处理，任务设计由易到难，并关注到利用“虚拟学校”网络平台，打破了传统课堂听力一锅端的现象，学生可以依据自己的进度认真完成听力练习，做到真正听到，听会，自主学习，让能走的走起来，能跑的跑起来，能飞的飞起来。",
    "mudule": [{
      name: '案例概述',
      path: '中学/fscommand/main/al001/main/01.htm'
    },{
      name: '案例亮点',
      path: '中学/fscommand/main/al001/main/02.htm'
    },{
      name: '教学反思',
      path: '中学/fscommand/main/al001/main/03.htm'
    },{
      name: '案例实录',
      path: '中学/fscommand/main/al001/main/sl.htm'
    }], 
  },
   {
    "type": '2', // 1是 前沿动态， 2是应用案例， 3应用感悟
    "id": 5,
    "name": "展示最精彩的自我——微写作讲评",
    "teacherName": "张斌",
    "subject": "语文",
    "grade": "初中",
    "schoolName": "昌平一中",
    "folderName": 'al002',
    "imgPath": "中学/fscommand/main/al002/main/img/fm.jpg",
    "intro": "充分利用虚拟学校平台，组织大量的书写与点评活动，让同学们实时互动，想看就看，并且老师和学生可以随时关注任何一个学生，这是传统课堂无法完成的。",
    "mudule":  [{
      name: '案例概述',
      path: ''
    },{
      name: '案例亮点',
      path: ''
    },{
      name: '教学设计',
      path: ''
    },{
      name: '教学反思',
      path: ''
    },{
      name: '专家点评',
      path: ''
    },{
      name: '案例实录',
      path: ''
    }], 
  },
   {
    "type": '2', // 1是 前沿动态， 2是应用案例， 3应用感悟
    "id": 6,
    "name": "extreme sports",
    "teacherName": "刘琳",
    "subject": "英语",
    "grade": "初中",
    "schoolName": "昌平一中",
    "folderName": 'al003',
    "imgPath": "中学/fscommand/main/al003/main/img/fm.jpg",
    "intro": "本节课的优点是听的环节整体处理，任务设计由易到难，并关注到了学生的自主性和个性需求。作为一个英语教师，听说课要做好文本处理和内化，把“听”与“说”充分结合。",
    "mudule":  [{
      name: '案例概述',
      path: ''
    },{
      name: '案例亮点',
      path: ''
    },{
      name: '教学设计',
      path: ''
    },{
      name: '教学反思',
      path: ''
    },{
      name: '专家点评',
      path: ''
    },{
      name: '案例实录',
      path: ''
    }], 
  },
  {
    "type": '2', // 1是 前沿动态， 2是应用案例， 3应用感悟
    "id": 7,
    "name": "利用昌平虚拟学校网络平台实施散文读写一体化专题教学的研究",
    "teacherName": "吴丽梅",
    "subject": "语文",
    "grade": "初中",
    "schoolName": "昌平一中",
    "folderName": 'al004',
    "imgPath": "中学/fscommand/main/al004/img/fm.jpg",
    "intro": "读写一体化专题教学是语文教学的一种教学形式，但在实际教学中，实施起来并不容易。而昌平虚拟学校网络平台在这方面为读写一体化专题教学提供了可能。",
    "mudule": [], 
  },
   {
    "type": '2', // 1是 前沿动态， 2是应用案例， 3应用感悟
    "id": 17,
    "name": "大牧场放牧业",
    "teacherName": "贺翔宇",
    "subject": "地理",
    "grade": "高中",
    "schoolName": "首都师范大学附属中学昌平学校",
    "folderName": 'al005',
    "imgPath": "中学/fscommand/main/al005/main/img/fm.jpg",
    "intro": "本堂课采用落实概念教学的方法，运用互动课堂对新授课进行了一次探索和尝试，意在挖掘其有利于提高地理教学实效性的功能。",
    "mudule":  [{
      name: '案例概述',
      path: ''
    },{
      name: '案例亮点',
      path: ''
    },{
      name: '教学反思',
      path: ''
    },{
      name: '案例实录',
      path: ''
    }], 
  },
   {
    "type": '2', // 1是 前沿动态， 2是应用案例， 3应用感悟
    "id": 18,
    "name": "带上她的眼睛",
    "teacherName": "杨爱清",
    "subject": "语文",
    "grade": "初中",
    "schoolName": "兴寿学校",
    "folderName": 'al006',
    "imgPath": "中学/fscommand/main/al006/main/img/fm.jpg",
    "intro": "如何充分激发学生读与写的热情，有效扩大学生的阅读量和写作量？如何开放学生的大脑，展开丰富的想象与创新？如何把“以学生为主体、教师为主导”的理念落到实处？本课以“双课堂教学平台”为工具，采用“双课堂”的教学理念，基本解决了这些问题。",
    "mudule":  [{
      name: '案例概述',
      path: ''
    },{
      name: '案例亮点',
      path: ''
    },{
      name: '教学设计',
      path: ''
    },{
      name: '教学反思',
      path: ''
    },{
      name: '专家点评',
      path: ''
    },{
      name: '案例实录',
      path: ''
    }], 
  },
   {
    "type": '2', // 1是 前沿动态， 2是应用案例， 3应用感悟
    "id": 19,
    "name": "情境写作——美丽记忆",
    "teacherName": "冷昊",
    "subject": "语文",
    "grade": "初中",
    "schoolName": "马池口中学",
    "folderName": 'al007',
    "imgPath": "中学/fscommand/main/al007/main/img/fm.jpg",
    "intro": "如何充分激发学生读与写的热情，有效扩大学生的阅读量和写作量？如何开放学生的大脑，展开丰富的想象与创新？如何把“以学情境写作也叫任务型表达，给学生设定具体情境之后，布置具体的任务，联系相应情境，选择有序而有针对性的表达并在很快的时间内完成任务要求。如何利用现代信息化技术手段来上情境写作课呢？",
    "mudule":  [{
      name: '案例概述',
      path: ''
    },{
      name: '案例亮点',
      path: ''
    },{
      name: '教学设计',
      path: ''
    },{
      name: '教学反思',
      path: ''
    },{
      name: '专家点评',
      path: ''
    },{
      name: '案例实录',
      path: ''
    }], 
  },
  {
    "type": '2', // 1是 前沿动态， 2是应用案例， 3应用感悟
    "id": 19,
    "name": "四世同堂里的人物形象",
    "teacherName": "曹敏",
    "subject": "语文",
    "grade": "高中",
    "schoolName": "首都师范大学附属中学昌平学校",
    "folderName": 'al008',
    "imgPath": "中学/fscommand/main/al008/main/img/fm.jpg",
    "intro": "面对时代久远的大厚本《四世同堂》，他们开始变得望而生畏。但虚拟学校却轻轻松松地解决了这一难题。在每天让学生批注式阅读的同时，拓展式阅读，知识竞答赛，讲有趣故事，人物点评，画人物画故事，寻访老舍故居，寻访小羊圈胡同一系列主题活动。让学生沉浸其中，各显风采。",
    "mudule":  [{
      name: '案例概述',
      path: ''
    },{
      name: '案例亮点',
      path: ''
    },{
      name: '教学设计',
      path: ''
    },{
      name: '教学反思',
      path: ''
    },{
      name: '专家点评',
      path: ''
    },{
      name: '案例实录',
      path: ''
    }], 
  },
 {
    "type": '2', // 1是 前沿动态， 2是应用案例， 3应用感悟
    "id": 19,
    "name": "神经系统的组成",
    "teacherName": "张春勇",
    "subject": "生物",
    "grade": "初中",
    "schoolName": "亭自庄学校",
    "folderName": 'al009',
    "imgPath": "中学/fscommand/main/al009/main/img/fm.jpg",
    "intro": "利用pad增加学生获取知识的有效途径，面向全体学生，提高生物科学素养，倡导探究性学习。倡导“学生主体生本课堂 师生共进”教学理念，引导学生主动参与和体验科学探究活动。使学生的学科素养在课上得到初步落实与体现。",
    "mudule":  [{
      name: '案例概述',
      path: ''
    },{
      name: '案例亮点',
      path: ''
    },{
      name: '教学设计',
      path: ''
    },{
      name: '案例实录',
      path: ''
    }], 
  },








  //小学
   {
    "type": '2', // 1是 前沿动态， 2是应用案例， 3应用感悟
    "id": 10,
    "name": "方阵问题",
    "imgPath": "小学/fscommand/main/al001/main/img/fm.jpg",
    "teacherName": "钟博婵",
    "subject": "数学",
    "grade": "小学",
    "schoolName": "南口学校",
    "intro": "教学内容更加丰富、灵活、生动，学生思维的角度更加宽泛，真正实现了学生自主学习和师生之间的信息交换，变更了现有的学习方式。",
    "folderName": 'al001',
    "mudule":  [{
      name: '案例概述',
      path: ''
    },{
      name: '案例亮点',
      path: ''
    },{
      name: '教学设计',
      path: ''
    },{
      name: '教学反思',
      path: ''
    },{
      name: '专家点评',
      path: ''
    },{
      name: '案例实录',
      path: ''
    }], 
  },
   {
    "type": '2', // 1是 前沿动态， 2是应用案例， 3应用感悟
    "id": 11,
    "name": "搭石",
    "imgPath": "小学/fscommand/main/al002/main/img/fm.jpg",
    "teacherName": "赵德凤",
    "subject": "语文",
    "grade": "小学",
    "schoolName": "二毛学校",
    "intro": "本案例充分借助同方知好乐电子书包教学平台，课前组织学生带着问题进行自主学习和探究，课堂上重点解决自学产生的问题并开展深层次的活动。让整个课堂更有效率和趣味性的同时，强化和巩固了学生的知识掌握程度。",
    "folderName": 'al002',
    "mudule":  [{
      name: '案例概述',
      path: ''
    },{
      name: '案例亮点',
      path: ''
    },{
      name: '教学设计',
      path: ''
    },{
      name: '教学反思',
      path: ''
    },{
      name: '案例实录',
      path: ''
    }],  
  },
   {
    "type": '2', // 1是 前沿动态， 2是应用案例， 3应用感悟
    "id": 12,
    "name": "图形的平移",
    "imgPath": "小学/fscommand/main/al003/main/img/fm.jpg",
    "teacherName": "姚辉",
    "subject": "数学",
    "grade": "小学",
    "schoolName": "史各庄中心小学",
    "intro": "以学生前测生成引发冲突，发现问题，提出问题，利用动画教具资源，充分让学生操作，让学生在“玩”的过程中，分析问题，解决问题，以此来突破学习的重点、难点。",
    "folderName": 'al003',
    "mudule":  [{
      name: '案例概述',
      path: ''
    },{
      name: '案例亮点',
      path: ''
    },{
      name: '教学反思',
      path: ''
    },{
      name: '专家点评',
      path: ''
    },{
      name: '案例实录',
      path: ''
    }], 
  },
   {
    "type": '2', // 1是 前沿动态， 2是应用案例， 3应用感悟
    "id": 13,
    "name": "平移",
    "imgPath": "小学/fscommand/main/al004/main/img/fm.jpg",
    "teacherName": "钟博婵",
    "subject": "数学",
    "grade": "小学",
    "schoolName": "南口学校",
    "intro": "把学生真实的学习过程展现出来，能让学生做到发现问题，提出问题，分析问题，解决问题。",
    "folderName": 'al004',
    "mudule":  [{
      name: '案例概述',
      path: ''
    },{
      name: '案例亮点',
      path: ''
    },{
      name: '教学设计',
      path: ''
    },{
      name: '教学反思',
      path: ''
    },{
      name: '专家点评',
      path: ''
    },{
      name: '案例实录',
      path: ''
    }],  
  },
   {
    "type": '2', // 1是 前沿动态， 2是应用案例， 3应用感悟
    "id": 14,
    "name": "can you tell me the way",
    "imgPath": "小学/fscommand/main/al005/main/img/fm.jpg",
    "teacherName": "刘佳慧",
    "subject": "英语",
    "grade": "小学",
    "schoolName": "天通苑小学",
    "intro": "以复习检测的形式导入，让学生通过自己做题，复习之前两节课的重点句型。整节课始终以Lady第一次来天通苑为主线进行讲授，十分贴近生活，让学生更能够身临其境地体会学习内容。",
    "folderName": 'al005',
    "mudule":  [{
      name: '案例概述',
      path: ''
    },{
      name: '案例亮点',
      path: ''
    },{
      name: '教学设计',
      path: ''
    },{
      name: '教学反思',
      path: ''
    },{
      name: '专家点评',
      path: ''
    },{
      name: '案例实录',
      path: ''
    }],  
  }, {
    "type": '2', // 1是 前沿动态， 2是应用案例， 3应用感悟
    "id": 20,
    "name": "生命生命",
    "imgPath": "小学/fscommand/main/al006/main/img/fm.jpg",
    "teacherName": "赵德凤",
    "subject": "语文",
    "grade": "小学",
    "schoolName": "二毛学校",
    "intro": "通过“由题入手，关注生命”“品读词句，感受生命”“拓展资料，情感升华”“回归课题，感悟生命”四个层次引导学生由浅入深地品词析句，读出自己的感受，以课堂为起点引发孩子们对生命的感悟。",
    "folderName": 'al006',
    "mudule": [{
      name: '案例概述',
      path: ''
    },{
      name: '案例亮点',
      path: ''
    },{
      name: '教学反思',
      path: ''
    },{
      name: '案例实录',
      path: ''
    }], 
  },{
    "type": '2', // 1是 前沿动态， 2是应用案例， 3应用感悟
    "id": 20,
    "name": "植树问题",
    "imgPath": "小学/fscommand/main/al007/main/img/fm.jpg",
    "teacherName": "汤淑芝",
    "subject": "数学",
    "grade": "小学",
    "schoolName": "二毛学校",
    "intro": "结合实际情境，体验发现和提出问题、分析和解决问题的过程。通过应用和反思，进一步理解所用的知识和方法，了解所学知识间的联系，获得数学活动经验。",
    "folderName": 'al007',
    "mudule": [{
      name: '案例概述',
      path: ''
    },{
      name: '案例亮点',
      path: ''
    },{
      name: '教学反思',
      path: ''
    },{
      name: '案例实录',
      path: ''
    }], 
  },
   {
    "type": '3', // 1是 前沿动态， 2是应用案例， 3应用感悟
    "id": 15,
    "name": "在虚拟与现实中跨越语文两极之美",
    "imgPath": "小学/fscommand/main/yj001/img/07.jpg",
    "teacherName": "张凌云",
    "subject": "语文",
    "grade": "小学",
    "schoolName": "回龙观第二小学",
    "intro": "学生通过这种新的学习方式，在轻松快乐的环境中参与语文活动，学习语文知识，感悟语文魅力。虚拟学校融入现实课堂的探索还在不断进行中，它倡导了开放而交互的学习方式，促使教师和学生共同学习，真正把课堂还给学生，让学生做学习的主人。",
    "folderName": 'yj001',
    "mudule": [], 
  },
   {
    "type": '3', // 1是 前沿动态， 2是应用案例， 3应用感悟
    "id": 16,
    "name": "虚拟课堂在语文教学中的应用感悟",
    "imgPath": "小学/fscommand/main/yj002/img/05.jpg",
    "teacherName": "陈婧",
    "subject": "语文",
    "grade": "小学",
    "schoolName": "平西府中心小学",
    "intro": "在语文教学中应用“双课堂”的教学模式，是因为它切实提高了学生在语文课堂中的学习效率和学习兴趣，相对于传统教学模式有一定的先进性，我们并不是为了用而用，也不是牵强附会地去用，更不是画蛇添足地用。",
    "folderName": 'yj002',
    "mudule": [], 
  } ,
  
   {
    "type": '3', // 1是 前沿动态， 2是应用案例， 3应用感悟
    "id": 21,
    "name": "以生为本虚实结合入心教育回归本源",
    "imgPath": "小学/fscommand/main/yj003/img/fm.jpg",
    "teacherName": "刚荣",
    "subject": "语文",
    "grade": "小学",
    "schoolName": "霍营中心小学",
    "intro": "以教师为中心的传统课堂授课模式，已越来越不能胜任在有限的课堂时间内传达越来越多的学科内容。“虚拟学校”教学平台不仅能提供图、文、声、像并茂的知识表达形式，传达更多的学科信息，还能提供不限时空、形象直观、界面友好的交互式学习环境。",
    "folderName": 'yj003',
    "mudule": [], 
  },
   {
    "type": '3', // 1是 前沿动态， 2是应用案例， 3应用感悟
    "id": 22,
    "name": "虚拟学校里的快乐行走",
    "imgPath": "小学/fscommand/main/yj004/img/fm.jpg",
    "teacherName": "王辉",
    "subject": "语文",
    "grade": "小学",
    "schoolName": "史各庄中心小学",
    "intro": "从最初的因年龄大和信息技术水平低而产生的抵触到如今的只要提起虚拟学校就要侃侃而谈，我惊诧于自己的变化，更惊诧于它带给我的教学上的变化。或许，可以用一句话来概括自己的感受——虚拟学校里，我和孩子们正在快乐地“行走”着。",
    "folderName": 'yj004',
    "mudule": [], 
  }
]
