//学科
var type=['教学设计/教案', '课件/学案', '图片', '动画', '音频/视频', '作业/试卷'];
//产吕简介
var productIntro = "<p>源于课标，贴近教材，打造精品课堂教学资源。类型丰富，拿来即用，全面满足教师教学需求。</p>";
var resDetail = "<p>★高中版：语数外理化生政史地，共10万条。</p><p>★初中版：语数外理化生政史地，共11万条。</p><p>★小学版：语数外，共4万条。</p>";
//名师微课数据
//教学素材数据
var couseData = [
{
		name: '[小学语文]《自己的花是让别人看的》教学设计',
		type: '教学设计/教案',catogry: 'design',
		pdf:'001.pdf'
	},
    {
        name: '[小学数学]《小数的意义和读写法》（第1课时）教案',
        type: '教学设计/教案',catogry: 'design',
		pdf:'002.pdf'
	},
    {
        name: '[小学英语]Do you like pears（第一课时）教学设计',
    	type: '教学设计/教案',catogry: 'design',
		pdf:'003.pdf'
	},
    {
        name: '[初中物理]《熔化和凝固》教案',
    	type: '教学设计/教案',catogry: 'design',
		pdf:'004.pdf'
	},
    {
        name: '[初中生物]《输送血液的泵──心脏》教案',
		type: '教学设计/教案',catogry: 'design',
		pdf:'005.pdf'
	},
{
		name: '[初中政治]《财产留给谁》教案',
		type: '教学设计/教案',catogry: 'design',
		pdf:'006.pdf'
	},
{
		name: '[初中地理]《亚洲的自然环境》教学设计',
		type: '教学设计/教案',catogry: 'design',
		pdf:'007.pdf'
	},
{
		name: '[初中历史]《繁盛一时的隋朝》教学设计',
		type: '教学设计/教案',catogry: 'design',
		pdf:'008.pdf'
	},    
    {
        name: '[高中语文]《琵琶行（并序）》教学设计',
		type: '教学设计/教案',catogry: 'design',
		pdf:'009.pdf'
	},    
    {
    	name: '[高中数学]《余弦定理》教学设计',
		type: '教学设计/教案',catogry: 'design',
		pdf:'010.pdf'
	},
    {
    	name: '[高中英语]《A land of diversity 》一体化教学案',
		type: '教学设计/教案',catogry: 'design',
		pdf:'011.pdf'
	},
{
		name: '[高中化学]《化学能与电能（第1课时）》教案',
		type: '教学设计/教案',catogry: 'design',
		pdf:'012.pdf'
	},

{
		name: '[小学语文]《窃读记》课件',
		type: '课件/学案',catogry: 'course',
		pdf:'001.pdf'
	},
{
		name: '[小学数学]《三角形的特性（第2课时）》课件',
		type: '课件/学案',catogry: 'course',
		pdf:'002.pdf'
	},
{
        name: '[小学英语]《What are you doing（第1课时）》课件',
		type: '课件/学案',catogry: 'course',
		pdf:'012.pdf'
	},
    {
		name: '[初中英语]《Do you have a soccer ball-reading》课件',
		type: '课件/学案',catogry: 'course',
		pdf:'004.pdf'
	},
{
        name: '[初中化学]《物质的变化与性质》课件',
		type: '课件/学案',catogry: 'course',
		pdf:'007.pdf'
	},
{
		name: '[初中物理]《浮力的应用》课件',
		type: '课件/学案',catogry: 'course',
		pdf:'008.pdf'
	},
{
        name: '[初中地理]《地球和地球仪》课件',
    	type: '课件/学案',catogry: 'course',
		pdf:'003.pdf'
	},
{
       	name: '[高中语文]《林黛玉进贾府》课件',
		type: '课件/学案',catogry: 'course',
		pdf:'010.pdf'
	},
{
     name: '[高中数学]《平面向量的实际背景及基本概念》课件',
    	type: '课件/学案',catogry: 'course',
		pdf:'009.pdf'
	},
{
		name: '[高中生物]《光合作用的过程》课件',
		type: '课件/学案',catogry: 'course',
		pdf:'005.pdf'
	},
{
		name: '[高中历史]《对外开放格局的初步形成》课件',
		type: '课件/学案',catogry: 'course',
		pdf:'006.pdf'
	},{
	
	
		name: '[高中政治]《人民民主专政：本质是人民当家做主》课件',
		type: '课件/学案',catogry: 'course',
		pdf:'011.pdf'
	},
{
	
    	name: '[初中英语]Water cycle',
		type: '图片',
		thumpnail: '[初中英语]Water cycle.jpg',
		pic:'[初中英语]图片：Water cycle.jpg'
	},
    {
    	name: '[初中化学]过滤“二低” ',
		type: '图片',
		thumpnail: '[初中化学]过滤“二低”.jpg',
		pic:'[初中化学]图片：过滤“二低“.jpg'
	},
{
		name: '[初中地理]长白山天池',
		type: '图片',
		thumpnail: '[初中地理]长白山天池.jpg',
		pic:'[初中地理]图片：长白山天池.jpg'
	},
    
//{
//  	name: '[初中语文]《芦花荡》景物描写',
//		type: '动画',
//		thumpnail: '[初中语文]《芦花荡》景物描写.png',
//		flash:'[初中语文]动画：《芦花荡》景物描写.swf'
//	},
//{
//		name: '[初中数学]投圈游戏',
//		type: '动画',
//		thumpnail: '[初中数学]动画：投圈游戏.png',
//		flash:'[初中数学]动画：投圈游戏.swf'
//	},
//{
//		name: '[初中生物]被子植物的传粉和受精',
//		type: '动画',
//		thumpnail: '[初中生物]被子植物的传粉和受精.png',
//		flash:'[初中生物]动画：被子植物的传粉和受精.swf'
//	},

{
    	name: '[小学语文]《金色的鱼钩》学案',
		type: 'tutorial',
		catogry: 'tutorial',
		thumpnail: '[小学语文]学案：金色的鱼钩.jpg',
		pdf:'1.pdf'
	},
    {
    	name: ' [初中数学]《平行线及其判定》学案',
		type: 'tutorial',
		catogry: 'tutorial',
		thumpnail: '[初中数学]学案：《平行线及其判定》（第1课时）.jpg',
		pdf:'2.pdf'
	},
{
		name: '[高中物理]《生活中的圆周运动》学案',
		type: 'tutorial',
		catogry: 'tutorial',
		thumpnail: '[高中物理]学案：生活中的圆周运动.jpg',
		pdf:'3.pdf'
	},

{
		name: '[小学数学]音频：《公顷、平方千米》',
		type: '音频/视频',
		thumpnail: '[小学数学]人教新课标版《公顷、平方千米》音频素材.png',
		audio:'[小学数学]音频：人教新课标版《公顷、平方千米》.mp3'
	},
{
		name: '[小学英语]音频：Song Old MacDonald had a Farm',
		type: '音频/视频',
		thumpnail: '[小学英语]Song old macdonald had a farm.jpeg',
		audio:'[小学英语]音频：Song old macdonald had a farm.mp3'
	},
{
		name: '[初中语文]音频：《中国石拱桥》课文朗读',
		type: '音频/视频',
		thumpnail: '[初中语文]《中国石拱桥》课文朗读.jpg',
		audio:'[初中语文]音频：《中国石拱桥》课文朗读.mp3'
	},
    
    {
    	name: '[初中地理]视频：浩瀚无垠的太空',
		type: '音频/视频',
		thumpnail: '[初中地理]浩瀚无垠的太空.png',
		mp4:'[初中地理]视频：浩瀚无垠的太空.mp4'
	},
{
		name: '[初中政治]视频：神州十号宇航员太空授课',
		type: '音频/视频',
		thumpnail: '[初中政治]神州十号宇航员太空授课.png',
		mp4:'[初中政治]视频：神州十号宇航员太空授课.mp4'
	},
{
		name: '[高中历史]视频：大国崛起-哥伦布',
		type: '音频/视频',
		thumpnail: '[高中历史]大国崛起-哥伦布.png',
		mp4:'[高中历史]视频：大国崛起-哥伦布.mp4'
	},

{
    	name: '[小学语文]《草原》试卷',
		type: '作业/试卷',catogry: 'homework',
		pdf:'001.pdf'
	},
    {
    	name: '[小学数学]《大数的认识（第2课时）》课时达标',
		type: '作业/试卷',catogry: 'homework',
		pdf:'002.pdf'
	},
{
		name: '[小学英语]《My clothes Part A 》同步训练',
		type: '作业/试卷',catogry: 'homework',
		pdf:'003.pdf'
	},
{
    	name: '[初中语文]《亲爱的爸爸妈妈》作业',
		type: '作业/试卷',catogry: 'homework',
		pdf:'004.pdf'
	},
{
		name: '[初中数学]《勾股定理的逆定理》（第1课时）作业',
		type: '作业/试卷',catogry: 'homework',
		pdf:'005.pdf'
	},
{
		name: '[初中化学]《物质的变化与性质》作业',
		type: '作业/试卷',catogry: 'homework',
		pdf:'006.pdf'
	},
    {
    	name: '[初中生物]《人体内废物的排出》测试（A卷）',
		type: '作业/试卷',catogry: 'homework',
		pdf:'007.pdf'
	},
    {
    	name: '[初中历史]《对外友好往来（人教版）》试卷',
		type: '作业/试卷',catogry: 'homework',
		pdf:'008.pdf'
	},
    {
    	name: '[高中数学]试卷：《函数y＝Asin(ωx＋φ)的图象》（第2课时）课时达标',
		type: '作业/试卷',catogry: 'homework',
		pdf:'009.pdf'
	},
    {
    	name: '[高中物理]《曲线运动》作业',
		type: '作业/试卷',catogry: 'homework',
		pdf:'010.pdf'
	},

{
		name: '[高中化学]《化学平衡（第1课时）》课时检测',
		type: '作业/试卷',catogry: 'homework',
		pdf:'011.pdf'
	},
{
		name: '[高中地理]《人口的空间变化》试卷',
		type: '作业/试卷',catogry: 'homework',
		pdf:'012.pdf'
	}
]
//图片轮播
var slider = [{
	image: "滚图截图/[高中语文]动画：《将进酒》赏析.jpg",
	title: "[高中语文]动画：《将进酒》赏析",
	url: "滚图截图/[高中语文]动画：《将进酒》赏析.swf"
}, {
	image: "滚图截图/[高中化学]动画：硫酸事故处理.jpg",
	title: "[高中化学]动画：硫酸事故处理",
	url: "滚图截图/[高中化学]动画：硫酸事故处理.swf"
}, {
	image: "滚图截图/[初中语文]图片：卢沟晓月.jpg",
	title: "[初中语文]图片：卢沟晓月",
	url: "滚图截图/[初中语文]图片：卢沟晓月.png"
}, {
	image: "滚图截图/[初中政治]视频：“2014最美乡村教师”获得者朱敏才、孙丽娜.jpg",
	title: "[初中政治]视频：“2014最美乡村教师”获得者朱敏才、孙丽娜",
	url: "滚图截图/[初中政治]视频：“2014最美乡村教师”获得者朱敏才、孙丽娜.mp4"
}, {
	image: "滚图截图/[初中英语]课件：Colors—Writing.jpg",
	title: "[初中英语]课件：Colors—Writing",
	url: "sliderpdf/1.pdf"
}, {
	image: "滚图截图/[初中历史]万千气象的宋代社会风貌.jpg",
	title: "[初中历史]课件：万千气象的宋代社会风貌",
	url: "sliderpdf/2.pdf"
},
{
    image: "滚图截图/[高中生物]《叶绿素和其他色素》教学设计.jpg",
	title: "[高中生物]《叶绿素和其他色素》教学设计",
	url: "sliderpdf/3.pdf"
}, {
	image: "滚图截图/[高中物理]《牛顿第二定律》作业.jpg",
	title: "[高中物理]《牛顿第二定律》作业",
	url: "sliderpdf/4.pdf"
}]