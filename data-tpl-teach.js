﻿//学科
var type=['语文','数学','英语','物理','化学','生物','政治','历史','地理','信息技术','综合实践活动'];
//产吕简介
var productIntro = "<p>引领网络与多媒体教学，立体诠释教学最新理念</p><p>精选全国优秀案例，助力教师教学提升</p>";
var resDetail = "<p>高中版：语数外理化生政史地、信息技术、综合实践活动 11个学科，共900个。</p><p>初中版：语数外理化生政史地、信息技术、综合实践活动 11个学科，共400个。</p><p>小学版：语数外、信息技术、综合实践活动 5个学科，共300个。</p>";
//动画教具数据
var couseData = [
{
		name: '[小学]新型玻璃',
		type: '语文',
		thumpnail: '[小学]新型玻璃.png',
		flash:'[小学]新型玻璃/index.htm'
	},
    {
    	name: '[小学]临死前的严监生',
		type: '语文',
		thumpnail: '[小学]临死前的严监生.png',
		flash:'[小学]临死前的严监生/index.htm'
	},
{
		name: '[初中]从百草园到三味书屋',
		type: '语文',
		thumpnail: '[初中]从百草园到三味书屋.png',
		flash:'[初中]从百草园到三味书屋/index.htm'
	},
    {
        name: '[初中]苏州园林',
		type: '语文',
		thumpnail: '[初中]苏州园林.png',
		flash:'[初中]苏州园林/index.htm'
	},
{
		name: '[高中]开疆辟土探新知——《中国新诗百年》选修专题教学案例',
		type: '语文',
		thumpnail: '[高中]开疆辟土探新知——《中国新诗百年》选修专题教学案例.png',
		flash:'[高中]开疆辟土探新知——《中国新诗百年》选修专题教学案例/index.htm'
	},
{
		name: '[高中]祝福',
		type: '语文',
		thumpnail: '[高中]祝福.png',
		flash:'[高中]祝福/index.htm'
	},   

{
		name: '[小学]小数的初步认识',
		type: '数学',
		thumpnail: '[小学]小数的初步认识.png',
		flash:'[小学]小数的初步认识/index.htm'
	},
{
		name: '[小学]用除法解决问题',
		type: '数学',
		thumpnail: '[小学]用除法解决问题.png',
		flash:'[小学]用除法解决问题/index.htm'
	},
    {
        name: '[小学]探索规律——正方体涂色问题',
		type: '数学',
		thumpnail: '[小学]探索规律——正方体涂色问题.jpg',
		flash:'[小学]探索规律——正方体涂色问题/index.htm'
	},
{
		name: '[初中]三角形的边',
		type: '数学',
		thumpnail: '[初中]三角形的边.jpg',
		flash:'[初中]三角形的边/index.htm'
	},
{
		name: '[高中]简单的线性规划问题',
		type: '数学',
		thumpnail: '[高中]简单的线性规划问题.jpg',
		flash:'[高中]简单的线性规划问题/index.htm'
	},
    
{
		name: '[高中]用样本的频率分布估计总体的分布',
		type: '数学',
		thumpnail: '[高中]用样本的频率分布估计总体的分布.jpg',
		flash:'[高中]用样本的频率分布估计总体的分布/index.htm'
	},
{
		name: '[小学]Tony the Flying Turtle',
		type: '英语',
		thumpnail: '[小学] Tony the Flying Turtle.jpg',
		flash:'[小学] Tony the Flying Turtle/index.htm'
	},
    {
        name: '[小学] Seasons',
		type: '英语',
		thumpnail: '[小学] Seasons.jpg',
		flash:'[小学] Seasons/index.htm'
	},
{
		name: '[初中]We have a problem',
		type: '英语',
		thumpnail: '[初中]We have a problem.jpg',
		flash:'[初中]We have a problem/index.htm'
	},
    {
    	name: '[初中]Unit 4 Reading A —Educational visits',
		type: '英语',
		thumpnail: '[初中] Unit 4 Reading A —Educational visits.jpg',
		flash:'[初中]Unit 4 Reading A —Educational visits/index.htm'
	},
{
		name: '[高中]Festival',
		type: '英语',
		thumpnail: '[高中]Festival.jpg',
		flash:'[高中]Festival/index.html'
	},
{
		name: '[高中]基于互动模式阅读过程的任务型英语阅读课',
		type: '英语',
		thumpnail: '[高中]高中英语起始阅读课教学——基于互动模式阅读过程的任务型英语阅读课.jpg',
		flash:'[高中]高中英语起始阅读课教学——基于互动模式阅读过程的任务型英语阅读课/index.htm'
	},
    

{
		name: '[初中]大气压强',
		type: '物理',
		thumpnail: '[初中]大气压强.jpg',
		flash:'[初中]大气压强/index.htm'
	},
    {
        name: '[初中]机械能及其转化',
		type: '物理',
		thumpnail: '[初中]机械能及其转化.jpg',
		flash:'[初中]机械能及其转化/index.htm'
	},
{
		name: '[初中]探究凸透镜成像规律',
		type: '物理',
		thumpnail: '[初中]探究凸透镜成像规律.jpg',
		flash:'[初中]探究凸透镜成像规律/index.html'
	},
{
		name: '[高中]电场的叠加',
		type: '物理',
		thumpnail: '[高中]电场的叠加.jpg',
		flash:'[高中]电场的叠加/index.html'
	},
{
		name: '[高中]摩擦力',
		type: '物理',
		thumpnail: '[高中]摩擦力.jpg',
		flash:'[高中]摩擦力/index.html'
	},
{
		name: '[初中]二力平衡的条件',
		type: '物理',
		thumpnail: '[初中]二力平衡的条件.jpg',
		flash:'[初中]二力平衡的条件/index.html'
	},
    
{
		name: '[初中]实验探究：为什么木炭在氧气中燃烧不产生火焰现象',
		type: '化学',
		thumpnail: '[初中]实验探究：为什么木炭在氧气中燃烧不产生火焰现象.jpg',
		flash:'[初中]实验探究：为什么木炭在氧气中燃烧不产生火焰现象/index.htm'
	},
{
		name: '[高中]化学平衡',
		type: '化学',
		thumpnail: '[高中]化学平衡.jpg',
		flash:'[高中]化学平衡/index.htm'
	},
{
		name: '[高中]离子反应——借助手持技术，实现宏微观结合',
		type: '化学',
		thumpnail: '[高中]离子反应——借助手持技术，实现宏微观结合.jpg',
		flash:'[高中]离子反应——借助手持技术，实现宏微观结合/index.htm'
	},
    {
    	name: '[高中]难溶电解质溶解平衡专题精选课例',
		type: '化学',
		thumpnail: '[高中]难溶电解质溶解平衡专题精选课例.jpg',
		flash:'[高中]难溶电解质溶解平衡专题精选课例/index.htm'
	},
{
		name: '[高中]影响化学反应速率的因素',
		type: '化学',
		thumpnail: '[高中]影响化学反应速率的因素.jpg',
		flash:'[高中]影响化学反应速率的因素/index.htm'
	},
    {
    	name: '[高中]醛和酮的化学性质',
		type: '化学',
		thumpnail: '[高中]醛和酮的化学性质.jpg',
		flash:'[高中]醛和酮的化学性质/index.htm'
	},
{
		name: '[初中]光合作用发现史',
		type: '生物',
		thumpnail: '[初中]光合作用发现史.jpg',
		flash:'[初中]光合作用发现史/index.htm'
	},
{
		name: '[初中]鸟类适于飞行的特点',
		type: '生物',
		thumpnail: '[初中]鸟类适于飞行的特点.jpg',
		flash:'[初中]鸟类适于飞行的特点/index.htm'
	},
    {
        name: '[初中]细菌',
		type: '生物',
		thumpnail: '[初中]细菌.jpg',
		flash:'[初中]细菌/index.htm'
	},
{
		name: '[高中]来自生物技术的忧虑（浙科版）',
		type: '生物',
		thumpnail: '[高中]来自生物技术的忧虑（浙科版）.jpg',
		flash:'[高中]来自生物技术的忧虑（浙科版）/index.htm'
	},
{
		name: '[高中]免疫调节',
		type: '生物',
		thumpnail: '[高中]免疫调节.jpg',
		flash:'[高中]免疫调节/index.htm'
	},
{
		name: '[高中]DNA结构',
		type: '生物',
		thumpnail: '[高中]DNA结构.jpg',
		flash:'[高中]DNA结构/index.htm'
	},
    {
    	name: '[初中]维护国家安全',
		type: '政治',
		thumpnail: '[初中]维护国家安全.jpg',
		flash:'[初中]维护国家安全/index.htm'
	},

{
		name: '[初中]感悟青春',
		type: '政治',
		thumpnail: '[初中]感悟青春.jpg',
		flash:'[初中]感悟青春/index.htm'
	},

    {
        name: '[初中]自立自强',
		type: '政治',
		thumpnail: '[初中]自立自强.jpg',
		flash:'[初中]自立自强/index.htm'
	},
    {
    	name: '[初中]我的情绪我做主',
		type: '政治',
		thumpnail: '[初中]我的情绪我做主.jpg',
		flash:'[初中]我的情绪我做主/index.htm'
	},
{
		name: '[高中]政府的权力：依法行使',
		type: '政治',
		thumpnail: '[高中]政府的权力：依法行使.jpg',
		flash:'[高中]政府的权力：依法行使/index.htm'
	},
{
		name: '[高中]树立正确的消费观',
		type: '政治',
		thumpnail: '[高中]树立正确的消费观.jpg',
		flash:'[高中]树立正确的消费观/index.htm'
	},
{
		name: '[初中]唐朝的强盛',
		type: '历史',
		thumpnail: '[初中]唐朝的强盛.jpg',
		flash:'[初中]唐朝的强盛/index.htm'
	},
{
		name: '[高中]辛亥革命',
		type: '历史',
		thumpnail: '[高中]辛亥革命.jpg',
		flash:'[高中]辛亥革命/index.htm'
	},
{
		name: '[高中]北魏孝文帝改革与民族融合',
		type: '历史',
		thumpnail: '[高中]北魏孝文帝改革与民族融合.jpg',
		flash:'[高中]北魏孝文帝改革与民族融合/index.htm'
	},
{
		name: '[高中]诗歌、小说与戏剧——19世纪以来的世界文化',
		type: '历史',
		thumpnail: '[高中]诗歌、小说与戏剧——19世纪以来的世界文化.jpg',
		flash:'[高中]诗歌、小说与戏剧——19世纪以来的世界文化/index.htm'
	},
    {
    	name: '[高中]马克思主义的诞生',
		type: '历史',
		thumpnail: '[高中]马克思主义的诞生.jpg',
		flash:'[高中]马克思主义的诞生/index.htm'
	},
{
		name: '[高中]明朝中后期的社会经济',
		type: '历史',
		thumpnail: '[高中]明朝中后期的社会经济.jpg',
		flash:'[高中]明朝中后期的社会经济/index.htm'
	},
{
		name: '[高中]大规模的海水运动',
		type: '地理',
		thumpnail: '[高中]大规模的海水运动.jpg',
		flash:'[高中]大规模的海水运动/index.htm'
	},
{
		name: '[初中]海陆的变迁',
		type: '地理',
		thumpnail: '[初中]海陆的变迁.jpg',
		flash:'[初中]海陆的变迁/index.htm'
	},
{
		name: '[初中]祖国神圣的领土——台湾（第1课时）',
		type: '地理',
		thumpnail: '[初中]祖国神圣的领土——台湾（第1课时）.jpg',
		flash:'[初中]祖国神圣的领土——台湾（第1课时）/index.htm'
	},
    {
    	name: '[初中]长江',
		type: '地理',
		thumpnail: '[初中]长江.jpg',
		flash:'[初中]长江/index.htm'
	},
    {
        name: '[初中]地震的威胁',
		type: '地理',
		thumpnail: '[初中]地震的威胁.jpg',
		flash:'[初中]地震的威胁/index.htm'
	},
{
		name: '[高中]等高线专题复习（一）',
		type: '地理',
		thumpnail: '[高中]等高线专题复习（一）.jpg',
		flash:'[高中]等高线专题复习（一）/index.htm'
	},
{
		name: '[小学]有趣的Scratch',
		type: '信息技术',
		thumpnail: '[小学]有趣的Scratch.jpg',
		flash:'[小学]有趣的Scratch/index.htm'
	},
    {
    	name: '[小学]初识Scratch',
		type: '信息技术',
		thumpnail: '[小学]初识Scratch.jpg',
		flash:'[小学]初识Scratch/index.htm'
	},
{
		name: '[初中]游戏编程初体验',
		type: '信息技术',
		thumpnail: '[初中]游戏编程初体验.jpg',
		flash:'[初中]游戏编程初体验/index.htm'
	},
{
		name: '[高中]算法初探',
		type: '信息技术',
		thumpnail: '[高中]算法初探.jpg',
		flash:'[高中]算法初探/index.htm'
	},
{
		name: '[初中]Flash形状补间动画——Family',
		type: '信息技术',
		thumpnail: '[初中]Flash形状补间动画——Family.jpg',
		flash:'[初中]Flash形状补间动画——Family/index.htm'
	},
    {
    	name: '[高中]信息的编程加工——寻找情报的解密密码',
		type: '信息技术',
		thumpnail: '[高中]信息的编程加工——寻找情报的解密密码.jpg',
		flash:'[高中]信息的编程加工——寻找情报的解密密码/index.htm'
	},
{
		name: '[小学]体验扎染',
		type: '综合实践活动',
		thumpnail: '[小学]体验扎染.jpg',
		flash:'[小学]体验扎染/index.htm'
	},
{
		name: '[小学]扎西德勒',
		type: '综合实践活动',
		thumpnail: '[小学]扎西德勒.jpg',
		flash:'[小学]扎西德勒/index.htm'
	},
    {
    	name: '[小学]舰载机起飞的秘密',
		type: '综合实践活动',
		thumpnail: '[小学]舰载机起飞的秘密.jpg',
		flash:'[小学]舰载机起飞的秘密1/index.htm'
	},
{
		name: '[初中]构建双流中学实验学校学生家校交通网络图',
		type: '综合实践活动',
		thumpnail: '[初中]构建双流中学实验学校学生家校交通网络图.jpg',
		flash:'[初中]构建双流中学实验学校学生家校交通网络图/index.htm'
	},
    {
        name: '[初中]我爱航天',
		type: '综合实践活动',
		thumpnail: '[初中]我爱航天.jpg',
		flash:'[初中]我爱航天/index.htm'
	},
{
		name: '[高中]访谈技巧的设计与运用',
		type: '综合实践活动',
		thumpnail: '[高中]访谈技巧的设计与运用.jpg',
		flash:'[高中]访谈技巧的设计与运用/index.htm'
	}
    

]
//图片轮播
var sliderArray = [
{
	"img": "案例库/滚动大图/[初中语文]我的叔叔于勒.png",
	"title": "[初中语文]我的叔叔于勒",
	"url": "案例库/滚动大图/[初中语文]我的叔叔于勒/index.htm"
},
{
	"img": "案例库/滚动大图/[初中数学]《一元一次不等式与不等式组》复习.jpg",
	"title": "[初中数学]《一元一次不等式与不等式组》复习",
	"url": "案例库/滚动大图/[初中数学]《一元一次不等式与不等式组》复习/index.htm"
},
{
	"img": "案例库/滚动大图/[小学英语]What shape is this.jpg",
	"title": "[小学英语]What shape is this",
	"url": "案例库/滚动大图/[小学英语]What shape is this/index.htm"
},
{
	"img": "案例库/滚动大图/[高中物理]自由落体运动.jpg",
	"title": "[高中物理]自由落体运动",
	"url": "案例库/滚动大图/[高中物理]自由落体运动/index.htm"
},
{
	"img": "案例库/滚动大图/[高中化学]化学能与电能的相互转化.jpg",
	"title": "[高中化学]化学能与电能的相互转化",
	"url": "案例库/滚动大图/[高中化学]化学能与电能的相互转化/index.htm"
},
{
	"img": "案例库/滚动大图/[高中生物]种群数量的变化.jpg",
	"title": "[高中生物]种群数量的变化",
	"url": "案例库/滚动大图/[高中生物]种群数量的变化/index.htm"
},
{
	"img": "案例库/滚动大图/[高中政治]文化创新的途径.jpg",
	"title": "[高中政治]文化创新的途径",
	"url": "案例库/滚动大图/[高中政治]文化创新的途径/index.htm"
},
{
	"img": "案例库/滚动大图/[初中历史]资本主义时代的曙光——文艺复兴.jpg",
	"title": "[初中历史]资本主义时代的曙光——文艺复兴",
	"url": "案例库/滚动大图/[初中历史]资本主义时代的曙光——文艺复兴/index.htm"
},
{
	"img": "案例库/滚动大图/[初中地理]海底地形的形成.jpg",
	"title": "[初中地理]海底地形的形成",
	"url": "案例库/滚动大图/[初中地理]海底地形的形成/index.htm"
},
{
	"img": "案例库/滚动大图/[高中信息技术]信息的鉴别与评价.jpg",
	"title": "[高中信息技术]信息的鉴别与评价",
	"url": "案例库/滚动大图/[高中信息技术]信息的鉴别与评价/index.htm"
},
{
	"img": "案例库/滚动大图/[初中综合实践活动]我家我设计.jpg",
	"title": "[初中综合实践活动]我家我设计",
	"url": "案例库/滚动大图/[初中综合实践活动]我家我设计/index.htm"
}
]