﻿//教辅库
//学科
var subject=['语文', '数学', '英语', '化学', '物理'];
//产吕简介
var productIntro = "<p>依托纸质教辅书思路，利用多媒体合成技术，立体呈现内容</p><p>名师助阵、拓展延伸，答疑解惑，动态辅助教学</p>";
var resDetail = "<p>高中版：语数外理化 5个学科，共500本。</p><p>初中版：语数外、物理 4个学科，共400本。</p>";
//多媒体教辅数据
var couseData = [
        {
		name: '[高中]氧化还原反应',
		subject: '化学',
		thumpnail: '[高中]氧化还原反应.png',
		ebook:'index.html'
	},{
    	name: '[高中]化学实验室安全',
		subject: '化学',
		thumpnail: '[高中]化学实验室安全.png',
		ebook:'index.html'
	},
        {
		name: '[高中]醇',
		subject: '化学',
		thumpnail: '[高中]醇.png',
		ebook:'index.html'
	},{
    	name: '[高中]有机物的命名',
		subject: '化学',
		thumpnail: '[高中]有机物的命名.png',
		ebook:'index.html'
	},
        {
		name: '[高中]专题：燃烧热和中和热',
		subject: '化学',
		thumpnail: '[高中]专题：燃烧热和中和热.png',
		ebook:'index.html'
	},
        {
		name: '[高中]专题：酸碱中和滴定及应用',
		subject: '化学',
		thumpnail: '[高中]专题：酸碱中和滴定及应用.png',
		ebook:'index.html'
	},  
       {
		name: '[高中]专题：化学反应热的计算',
		subject: '化学',
		thumpnail: '[高中]专题：化学反应热的计算.png',
		ebook:'index.html'
	},
       {
		name: '[高中]专题：原电池与电解池的区别',
		subject: '化学',
		thumpnail: '[高中]专题：原电池与电解池的区别.png',
		ebook:'index.html'
	},
       {
		name: '[高中]椭圆',
		subject: '数学',
		thumpnail: '[高中]椭圆.jpg',
		ebook:'index.html'
	},
        {
		name: '[高中]数列通项与求和',
		subject: '数学',
		thumpnail: '[高中]数列通项与求和.jpg',
		ebook:'index.html'
	}, {
    	name: '[高中]直线与平面垂直的判定及性质',
		subject: '数学',
		thumpnail: '[高中]直线与平面垂直的判定及性质.jpg',
		ebook:'index.html'
	},
        {
		name: '[高中]集合及其运算',
		subject: '数学',
		thumpnail: '[高中]集合及其运算.jpg',
		ebook:'index.html'
	},
        {
		name: '[高中]二项式定理',
		subject: '数学',
		thumpnail: '[高中]二项式定理.jpg',
		ebook:'index.html'
	},
        {
		name: '[初中]勾股定理',
		subject: '数学',
		thumpnail: '[初中]勾股定理.jpg',
		ebook:'index.html'
	},
        {
		name: '[初中]等腰三角形',
		subject: '数学',
		thumpnail: '[初中]等腰三角形.jpg',
		ebook:'index.html'
	},
        {
		name: '[初中]实际问题和一元一次方程',
		subject: '数学',
		thumpnail: '[初中]实际问题和一元一次方程.jpg',
		ebook:'index.html'
	},
        {
		name: '[初中]阿基米德原理',
		subject: '物理',
		thumpnail: '[初中]阿基米德原理.jpg',
		ebook:'index.html'
	},
        {
		name: '[初中]凸透镜成像的规律',
		subject: '物理',
		thumpnail: '[初中]凸透镜成像的规律.jpg',
		ebook:'index.html'
	}, {
    	name: '[高中]库仑定律',
		subject: '物理',
		thumpnail: '[高中]库仑定律.jpg',
		ebook:'index.html'
	}, {
    	name: '[高中]磁场对运动电荷的作用力',
		subject: '物理',
		thumpnail: '[高中]磁场对运动电荷的作用力.jpg',
		ebook:'index.html'
	},{
    	name: '[高中]外力作用下的振动',
		subject: '物理',
		thumpnail: '[高中]外力作用下的振动.jpg',
		ebook:'index.html'
	},
        {
		name: '[高中]反冲运动　火箭',
		subject: '物理',
		thumpnail: '[高中]反冲运动　火箭.jpg',
		ebook:'index.html'
	},
        {
		name: '[高中]质点在平面内的运动',
		subject: '物理',
		thumpnail: '[高中]质点在平面内的运动.jpg',
		ebook:'index.html'
	},
        {
		name: '[高中]动能和动能定理',
		subject: '物理',
		thumpnail: '[高中]动能和动能定理.jpg',
		ebook:'index.html'
	},
       {
		name: '[初中]Have you ever been to a museum ( I )',
		subject: '英语',
		thumpnail: '[初中]Have you ever been to a museum ( I ).jpg',
		ebook:'index.html'
	},
        {
		name: '[初中]Is there a post office near here(Ⅱ)',
		subject: '英语',
		thumpnail: '[初中]Is there a post office near here(Ⅱ).jpg',
		ebook:'index.html'
	},
        {
		name: '[初中]What’s the matter(I)',
		subject: '英语',
		thumpnail: '[初中]What’s the matter ( I ).jpg',
		ebook:'index.html'
	},
        {
		name: '[初中]What did you do last weekend(Ⅰ)',
		subject: '英语',
		thumpnail: '[初中]What did you do last weekend (Ⅰ).jpg',
		ebook:'index.html'
	}, {
    	name: '[高中]Conflict (I)',
		subject: '英语',
		thumpnail: '[高中]Conflict (I).jpg',
		ebook:'index.html'
	},
        {
		name: '[高中]Beauty(I)',
		subject: '英语',
		thumpnail: '[高中]Beauty(I).jpg',
		ebook:'index.html'
	},
        {
		name: '[高中]People(I)',
		subject: '英语',
		thumpnail: '[高中]People(I).jpg',
		ebook:'index.html'
	},
        {
		name: '[高中]Stories(I)',
		subject: '英语',
		thumpnail: '[高中]Stories(I).jpg',
		ebook:'index.html'
	},
        {
		name: '[高中]点击高考',
		subject: '语文',
		thumpnail: '[高中]点击高考.jpg',
		ebook:'index.html'
	},
        {
		name: '[高中]拓展阅读：文无定格 贵在鲜活',
		subject: '语文',
		thumpnail: '[高中]拓展阅读：文无定格 贵在鲜活.jpg',
		ebook:'index.html'
	},
        {
		name: '[高中]动画助学：沉静之美',
		subject: '语文',
		thumpnail: '[高中]动画助学：沉静之美.jpg',
		ebook:'index.html'
	},
        {
		name: '[高中]习作借鉴：结构合理：凤楼龙阁珠翠绕',
		subject: '语文',
		thumpnail: '[高中]习作借鉴：结构合理：凤楼龙阁珠翠绕.jpg',
		ebook:'index.html'
	},
        {
		name: '[初中]名著选介：红楼梦',
		subject: '语文',
		thumpnail: '[初中]名著选介：红楼梦.jpg',
		ebook:'index.html'
	},
       {
		name: '[初中]文史便览：中国传统文房四宝',
		subject: '语文',
		thumpnail: '[初中]文史便览：中国传统文房四宝.jpg',
		ebook:'index.html'
	}, {
    	name: '[初中]诵读欣赏：园林之趣',
		subject: '语文',
		thumpnail: '[初中]诵读欣赏：园林之趣.jpg',
		ebook:'index.html'
	},
        {
		name: '[初中]佳作共赏：战地一瞥',
		subject: '语文',
		thumpnail: '[初中]佳作共赏：战地一瞥.jpg',
		ebook:'index.html'
	}
]

//图片轮播
var sliderArray = [{
	"img": "语文/[高中语文]课堂延伸：名家名篇品鉴.jpg",
	"title": "[高中语文]课堂延伸：名家名篇品鉴",
	"url": "语文/[高中语文]课堂延伸：名家名篇品鉴/index.html"
}, {
	"img": "数学/[高中数学]三角函数的图象与性质.jpg",
	"title": "[高中数学]三角函数的图象与性质",
	"url": "数学/[高中数学]三角函数的图象与性质/index.html"
}, {
	"img": "英语/[初中英语]Don't eat in class(Ⅰ).jpg",
	"title": "[初中英语]Don't eat in class(Ⅰ)",
	"url": "英语/[初中英语]Don't eat in class(Ⅰ)/index.html"
}, {
	"img": "物理/[初中物理]测量物质的密度.jpg",
	"title": "[初中物理]测量物质的密度",
	"url": "物理/[初中物理]测量物质的密度/index.html"
}, {
	"img": "化学/[高中化学]元素周期表及其应用.jpg",
	"title": "[高中化学]元素周期表及其应用",
	"url": "化学/[高中化学]元素周期表及其应用/index.html"
}]